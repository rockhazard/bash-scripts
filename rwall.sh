#!/bin/bash

#===============================================================================
#
# FILE: rwall
#
# USAGE: rwall [-h] [-v] [-d1] [-d2] [-d3] [-dd] | IMAGE DIRECTORY
#
# DESCRIPTION:
# rWall automatically detects the current user's environment then selects a 
# random image from a directory and applies that image as a background. 
# It is written for GNOME3 (Gnome Shell, Unity, Cinnamon, Mate) and Openbox.
# Support for KDE4 is provided, and requires some trivial setup in KDE. 
# rWall may also work with other environments capable of using feh.
# 
# TODO: fuse with pixwalker.sh to enable file traversing
# OPTIONS: see function ’usage’ below
# REQUIREMENTS: feh for Openbox usage, and yad (ppa:webupd8team/y-ppa-manager)
# BUGS: ---
# NOTES: when running KDE, a template folder is created as ~/.config/rwall
# AUTHOR: Ike Davis, ike.davis.net@gmail.com
# COMPANY: ---
# VERSION: 2.0 "Krusty"
# CREATED: Thursday, May 2, 2013 12:30 PM
# REVISION: ---
# LICENSE: GPL 2.0
#===============================================================================

declare VERSION          # previously, v1.0 "Homer", v1.5 "Bart", v1.8 "Lisa"
declare BGDIR            # result of wallpaper commandline argument test
declare WALLPAPER        # result of randomized image selection
declare SETWALL          # command to change background of current desktop
declare USERDIR          # default image directory
declare INTERVAL         # number of times to loop background
declare SPEED            # number of seconds between background changes
declare DE               # tag for current desktop environment

VERSION="v2.0 \"Krusty\" (c) 2013, by Ike Davis"

#HELP
usage() {
  echo " rwall [-h] [-v] [-d1] [-d2] [-d3] [-dd] [-l] | IMAGE DIRECTORY"
  echo
  echo " --help .................. prints this help"
  echo " --version, -v ........... prints version number"
  echo " --alt-dir1, -d1 ......... use first alternate image directory"
  echo " --alt-dir2, -d2 ......... use second alternate image directory"
  echo " --alt-dir3, -d3 ......... use third alternate image directory"
  echo " --dialog, -dd ........... choose an image directory via a GTK dialog"
  echo " --loop, -l .............. loop background a given number of times"
  echo "                           every given number of seconds"
  echo
  echo " KDE users: Please manually set wallpaper to images in ~/.config/rwall"
  echo " after first run.  If you wish to use two different images on your"
  echo " monitors, just set them separately for each desktop."
  echo 
}

# PRE-DEFINED DIRECTORIES

# change the directory below to the desired default image directory
USERDIR=$HOME/path/to/primary/images
if [[ ! -d $USERDIR ]]; then
  echo "Please choose a valid default image directory!"
  exit
fi

dir_test() {
  if [[ ! -d $BGDIR ]]; then
  echo "Please provide a valid alternate image directory!"
  exit
  fi
}

# set alternate image directories below, then use options "-d1" through "-d3"
set_directory1() {
  BGDIR=$HOME/second/path/to/primary/images
  dir_test
}

set_directory2() {
  BGDIR=$HOME/third/path/to/primary/images
  dir_test
}

set_directory3() {
  BGDIR=$HOME/fourth/path/to/primary/images
  dir_test
}

# GUI DIALOGS

test_dialog() {
  if [[ ! -x /usr/bin/yad || ! -x /usr/bin/zenity ]]; then
    echo "Please install either yad or zenity to employ gui dialogs."
    exit
  fi
}

cancel_dialog() {
  if [[ $BGDIR == '' ]]; then
    exit
  fi
}

# Zenity dialog for choosing image directory
call_zenity_dialog() {
  test_dialog
  BGDIR=$(zenity --file-selection --directory --title='Pick Your Wallpaper Directory' \
  --filename=$HOME/Pictures/ --width=600 --height=500)
  cancel_dialog  
}

# Yad GTK dialog for choosing image directory
call_dir_dialog() { 
  test_dialog
  BGDIR=$(yad --file --directory --title='Pick Your Wallpaper Directory' \
  --borders=3 --filename=$HOME/Pictures/* --add-preview --width=600 --height=500)
  cancel_dialog
}

# Yad GTK dialog for picking image file
call_file_dialog() {
  test_dialog
  BGDIR=$(yad --file --title='Pick Your Wallpaper Image' \
  --borders=10 --add-preview --filename="$HOME/Pictures/*" --width=700 \
  --height=500)
  cancel_dialog
}

# SET DESKTOP ENVIRONMENTS

set_kde() {
  # create rWall settings folder, if it doesn't exist
  if [[ ! -d $HOME/.config/rwall ]]; then
    mkdir $HOME/.config/rwall &> /dev/null
  fi

  # first monitor
  # set KDE wallpaper to path below
  cp $WALLPAPER $HOME/.config/rwall/rwall-kde.jpg

  # second monitor
  # uncomment a set_directory to use a different image than the first monitor
  # set_directory1
  # set_directory2
  set_directory3
  random_sort ; cp $WALLPAPER $HOME/.config/rwall/rwall-monitor2-kde.jpg
}

set_openbox() {
  # feh --bg-center "$WALLPAPER"
  feh --bg-scale "$WALLPAPER"
  # feh --bg-tile "$WALLPAPER"
}

set_gnome3() {
  # options: centered, none, scaled, spanned, stretched, wallpaper, zoom
  gsettings set org.gnome.desktop.background picture-options 'scaled'

  # change "set"  to "get" and delete option to return current background image
  gsettings set org.gnome.desktop.background picture-uri file://"$WALLPAPER"
}

set_mate14() {
  mateconftool-2 -t str --set /desktop/mate/background/picture_filename "$WALLPAPER"

  # options; "none", "wallpaper" (tiled), "centered", "scaled", "stretched"
  mateconftool-2 -t str --set /desktop/mate/background/picture_options "scaled"
}

set_mate() {
  # options: centered, none, scaled, spanned, stretched, wallpaper, zoom
  gsettings set org.mate.background picture-options 'scaled'

  # change "set"  to "get" and delete option to return current background image
  gsettings set org.mate.background picture-filename "$WALLPAPER"
}

# randomize wallpaper list and select one image.  The purpose of the script.
random_sort() {
  WALLPAPER=$(find "$BGDIR" -type f -iregex ".*\.\(jpg\|png\|jpeg\)$" \
  | sort --random-sort | head --lines=1)
}

# LOOP WALLPAPER
loop () {
  # decide what directory to use
  call_dir_dialog
  # number of times to loop
  INTERVAL=$(yad --entry --text="Please enter an interval." --entry-text="5")
  # seconds between wallpaper change
  SPEED=$(yad --entry --text="Please enter a cycle speed." --entry-text="5")
  echo
  
  #default background swap speed
  if [[ -z $SPEED ]]; then
    SPEED=3
  fi

  if [[ -z $INTERVAL ]]; then
    echo "Press Ctrl+C to cancel the loop."
    echo
    while [[ true ]]; do
      random_sort
      $SETWALL
      sleep $SPEED
    done
  else
    echo "You have chosen to loop the background $INTERVAL times."
    echo "Press Ctrl+C to cancel the loop, or wait for the count to reach 0."
    echo
    echo "$(($INTERVAL - 1)) changes left."; echo
    while [[ $INTERVAL > 0 ]]; do
      random_sort
      $SETWALL
      sleep $SPEED
      let --INTERVAL
      if [[ $INTERVAL > 1 ]]; then
        echo "$(($INTERVAL - 1)) changes left."; echo
      elif [[ $INTERVAL == 1 ]]; then
        echo "Last change complete."; echo
      else
        echo "There are no changes left. Goodbye."; echo; exit 0
      fi
    done
  fi
}

cd /

# automatically set desktop environment variable for current user
if [[ $(w -sh | grep gnome | wc -l) == "1" ]]; then
  if [[ -x /usr/bin/gsettings ]]; then
    SETWALL=set_gnome3
    DE="gnome3"
  fi
elif [[ $(w -sh | grep gnome | wc -l) == "1" ]]; then
  if [[ -x /usr/bin/gconftool-2 ]]; then
    SETWALL=set_gnome2
    DE="gnome2"
  fi
elif [[ $(w -sh | grep mate | wc -l) == "1" ]]; then
  if [[ -x /usr/bin/mateconftool-2 ]]; then
    SETWALL=set_mate14
    DE="mate14"
  elif [[ -x /usr/bin/gsettings ]]; then
    SETWALL=set_mate
    DE="mate"
  fi
elif [[ $(w -sh | grep kdeinit | wc -l) == "1" ]]; then
  SETWALL=set_kde
  DE="kde"
elif [[ -x /usr/bin/feh ]]; then
  # feh can be used for many environments, so we set variable to "other"
  SETWALL=set_openbox
  DE="other"
else
  echo "Missing components!  Please ensure GNOME, KDE, or feh is installed."
fi

# establish image directory, includes provision for accepting options for $1
if [[ -d "$2" ]]; then
    BGDIR="$2"
elif [[ -d "$1" ]]; then
    BGDIR="$1"
else
    BGDIR="$USERDIR"
fi

random_sort

# OPTIONS
if [[ -d $2 || -z $2 ]]; then
  case $1 in
    "--help"             ) usage;;
    "--version" | "-v"   ) echo "$VERSION";;
    "--alt-dir1" | "-d1" ) set_directory1 ; random_sort ; $SETWALL;;
    "--alt-dir2" | "-d2" ) set_directory2 ; random_sort ; $SETWALL;;
    "--alt-dir3" | "-d3" ) set_directory3 ; random_sort ; $SETWALL;;
    "--d-dialog" | "-dd" ) call_dir_dialog ; random_sort ; $SETWALL;;
    "--f-dialog" | "-fd" ) call_file_dialog ; random_sort ; $SETWALL;;
    "--z-dialog" | "-zd" ) call_zenity_dialog ; random_sort ; $SETWALL;;
    ""                   ) $SETWALL;;
    "$BGDIR"             ) $SETWALL;;
    "--loop" | "-l"      ) loop;;
    "--redpill"          ) echo "Why oh why didn't I take the BLUE pill?";;
    "--bluepill"         ) echo "Never send a human to do a machine's job.";;
    *                    ) echo "\"$1\" is invalid. Type \"rwall --help.\""
  esac
fi

exit 0
